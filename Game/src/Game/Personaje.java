package Game;

public abstract class Personaje {

    private String nombre;
    private Integer energia;
    private String ataque;
    private Integer dannio;
    private Boolean estado;

    public Personaje(String nombre, Integer energia) {
        this.nombre = nombre;
        this.energia = energia;
        //Estado en falso para ambos personajes
        this.estado = false;
    }

    public void guardar(String rutaArchivo, String linea) {
        RWFile escribirEnArchivo = new RWFile();
        escribirEnArchivo.writefile(rutaArchivo, linea);
    }

    public void alimentarse(Integer nuevaEnergia) {
        this.energia += nuevaEnergia;
    }

    public void consumirEnergia(Integer gastoEnergia) {
        this.energia -= gastoEnergia;
    }

    public Boolean getEstado() {
        return this.estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getDannio() {
        return this.dannio;
    }

    public void setDannio(Integer dannio) {
        this.dannio = dannio;
    }

    public Integer getEnergia() {
        return energia;
    }

    public void setEnergia(Integer energia) {
        this.energia = energia;
    }

    public String getAtaque() {
        return this.ataque;
    }

    public void setAtaque(String ataque) {
        this.ataque = ataque;
    }

    public abstract String toStringAtaque();

    public abstract String toStringDannio(Integer dannioRecibido);

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
