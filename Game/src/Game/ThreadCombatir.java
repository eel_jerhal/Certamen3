package Game;

public class ThreadCombatir extends Thread {

    private Guerrero guerrero;
    private Mago mago;

    public ThreadCombatir(Guerrero guerrero, Mago mago) {
        this.guerrero = guerrero;
        this.mago = mago;
    }

    public void run() {
        //Estado inicial del guerrero se activa al atacar
        guerrero.setEstado(true);
        //Si ambos personajes siguen con vida se puede combatir
        while (guerrero.getEnergia() > 0 && mago.getEnergia() > 0) {
            try {
                //Si el mago está atacando, se espera a que termine su turno
                while (mago.getEstado()) {
                    this.wait();
                }
                //El guerrero ataca
                guerrero.combatir();
                //Se le quita energía al guerrero dependiendo del daño realizado por el mago
                mago.consumirEnergia(guerrero.getDannio());
                //Se muestra por pantalla el ataque del guerrero
                System.out.println(guerrero.toStringAtaque());
                //Se guarda el ataque dentro del archivo ataques
                guerrero.guardar("../../Stats/ataque.csv", guerrero.toStringAtaque());
                //Y se guarda el daño realizado al mago en el archivo dannio
                mago.guardar("../../Stats/dannio.csv", mago.toStringDannio(guerrero.getDannio()));
                //El guerrero deja de atacar
                guerrero.setEstado(false);
                //Se le dice al mago que puede atacar
                mago.setEstado(true);
                //Y se le notifican a los hilos de que pueden dejar de esperar
                this.notifyAll();
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
