package Game;

import java.util.Random;

public class Mago extends Personaje {

    private String poder;

    public Mago(String nombre, String poder) {
        super(nombre, 100);
        this.poder = poder;
    }

    public String getPoder() {
        return poder;
    }

    public void setPoder(String poder) {
        this.poder = poder;
    }

    public synchronized void encantar() {
        Random rand = new Random();
        Integer ataque = rand.nextInt(4);
        switch (ataque) {
            case (0): {
                this.setAtaque("Llamarada");
                this.setDannio(20);
                this.consumirEnergia(2);
                break;
            }
            case (1): {
                this.setAtaque("Incendio");
                Integer superAtaque = rand.nextInt(2);
                switch (superAtaque) {
                    case (0): {
                        this.setDannio(35);
                        break;
                    }
                    case (1): {
                        this.setDannio(40);
                        break;
                    }
                }
                this.consumirEnergia(2);
                break;
            }
            case (2): {
                this.setAtaque("Explosion");
                this.setDannio(60);
                alimentarse(this.getDannio() * (int) 0.5);
                this.consumirEnergia(2);
                break;
            }
            case (3): {
                this.setAtaque("Pierde turno");
                this.setDannio(0);
                break;
            }
        }
    }

    @Override
    public String toStringAtaque() {
        return "Mago;" + this.getAtaque() + ";" + this.getDannio();
    }

    @Override
    public String toStringDannio(Integer dannioRecibido) {
        return "Mago;" + dannioRecibido;
    }
}
