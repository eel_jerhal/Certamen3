package Game;

public class ThreadEncantar extends Thread {

    private Guerrero guerrero;
    private Mago mago;

    public ThreadEncantar(Mago mago, Guerrero guerrero) {
        this.guerrero = guerrero;
        this.mago = mago;
        //Se reciben las instancias del mago y el guerrero
    }

    public void run() {
        //El estado inicial del mago se activa al atacar
        mago.setEstado(true);
        //Si ambos personajes siguen con vida se puede combatir
        while (mago.getEnergia() > 0 && guerrero.getEnergia() > 0) {
            try {
                //Si el guerrero está atacando, se espera a que termine su turno
                while (guerrero.getEstado()) {
                    this.wait();
                }
                //El mago ataca
                mago.encantar();
                //Se le quita energía al guerrero dependiendo del daño realizado por el mago
                guerrero.consumirEnergia(mago.getDannio());
                //Se muestra por pantalla el ataque del mago
                System.out.println(mago.toStringAtaque());
                //Se guarda el ataque dentro del archivo ataques
                mago.guardar("../../Stats/ataque.csv", mago.toStringAtaque());
                //Y se guarda el daño realizado al guerrero en el archivo dannio
                guerrero.guardar("../../Stats/dannio.csv", guerrero.toStringDannio(mago.getDannio()));
                //El mago deja de atacar
                mago.setEstado(false);
                //Se le dice al guerrero que puede atacar
                guerrero.setEstado(true);
                //Y se le notifican a los hilos de que pueden dejar de esperar
                this.notifyAll();
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
