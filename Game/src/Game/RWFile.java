package Game;

import java.io.*;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class RWFile {

    public synchronized void writefile(String filePath, String lineaAEstribir) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        //Se crea carpeta Stats si es que esta no existe.
        File carpeta = new File("../../Stats");
        if (!carpeta.exists()) {
            carpeta.mkdir();
        }
        try {
            //Se crea el archivo
            fichero = new FileWriter(filePath, true);
            pw = new PrintWriter(fichero);
            //Se escribe la linea dentro del archivo
            pw.println(lineaAEstribir);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) //Si el fichero existe, se cierra.
                {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void readfile(JTable tablaAtaque, JTable tablaDaño, String tipo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            /* Apertura del fichero y creacion de BufferedReader para poder
             hacer una lectura comoda (disponer del metodo readLine()).*/

            archivo = new File("../../Stats/ataque.csv");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            //Se crea modelo de tabla
            String linea;
            DefaultTableModel modelo = new DefaultTableModel();

            //Se agregan columnas al modelo
            modelo.addColumn("ATAQUE");
            modelo.addColumn("DAÑO REALIZADO");

            // Lectura del fichero
            while ((linea = br.readLine()) != null) {
                String[] registro = linea.split(";");
                if (tipo.equals(registro[0])) {
                    //Se agrega una fila con el arreglo de datos al modelo
                    modelo.addRow(new String[]{registro[1], registro[2]});
                }
            }
            //Se pasa el modelo a la tabla ataque
            tablaAtaque.setModel(modelo);

            //Se crea fichero para el otro archivo
            archivo = new File("../../Stats/dannio.csv");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            //Se pasa un modelo vacío a la variable
            modelo = new DefaultTableModel();

            //Se agregan las nuevas columnas
            modelo.addColumn("DAÑO RECIBIDO");

            // Lectura del fichero
            while ((linea = br.readLine()) != null) {
                String[] registro = linea.split(";");
                if (tipo.equals(registro[0])) {
                    //Se agrega una fila con el arreglo de datos al modelo
                    modelo.addRow(new String[]{registro[1]});
                }
            }
            //Se pasa el modelo a la tabla daño
            tablaDaño.setModel(modelo);

        } catch (IOException ex) {
            GUI.Error error = new GUI.Error();
            error.setVisible(true);
        } finally {
            try {
                if (null != br) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
