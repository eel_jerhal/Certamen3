package GUI;

import Game.Combate;
import Game.RWFile;

public class Interfaz extends javax.swing.JFrame {

    public Interfaz() {
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPaneAtaque = new javax.swing.JScrollPane();
        ataqueTabla = new javax.swing.JTable();
        botonMago = new javax.swing.JButton();
        botonGuerrero = new javax.swing.JButton();
        jScrollPaneDaño = new javax.swing.JScrollPane();
        danioTabla = new javax.swing.JTable();
        btnCombate = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Estadísticas del Combate");
        setBackground(new java.awt.Color(255, 255, 255));

        ataqueTabla.setAutoCreateRowSorter(true);
        ataqueTabla.setBackground(new java.awt.Color(204, 204, 255));
        ataqueTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "ATAQUE", "DAÑO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ataqueTabla.setSelectionBackground(new java.awt.Color(0, 0, 0));
        jScrollPaneAtaque.setViewportView(ataqueTabla);

        botonMago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/imgMago.jpg"))); // NOI18N
        botonMago.setToolTipText("Cargar tabla Mago");
        botonMago.setAutoscrolls(true);
        botonMago.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        botonMago.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonMago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMagoActionPerformed(evt);
            }
        });

        botonGuerrero.setForeground(new java.awt.Color(240, 240, 240));
        botonGuerrero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/imgGuerrero.png"))); // NOI18N
        botonGuerrero.setToolTipText("Cargar tabla Guerrero");
        botonGuerrero.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        botonGuerrero.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonGuerrero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuerreroActionPerformed(evt);
            }
        });

        danioTabla.setBackground(new java.awt.Color(255, 255, 204));
        danioTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "ATAQUE", "DAÑO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPaneDaño.setViewportView(danioTabla);

        btnCombate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/imgCombate.jpg"))); // NOI18N
        btnCombate.setToolTipText("Combatir");
        btnCombate.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        btnCombate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCombate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCombateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonMago, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPaneAtaque, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonGuerrero, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPaneDaño, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(20, 20, 20))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCombate, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonMago, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonGuerrero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btnCombate, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPaneAtaque, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPaneDaño, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void botonMagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMagoActionPerformed
        RWFile read = new RWFile();
        read.readfile(ataqueTabla, danioTabla, "Mago");
    }//GEN-LAST:event_botonMagoActionPerformed

    private void botonGuerreroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuerreroActionPerformed
        RWFile read = new RWFile();
        read.readfile(ataqueTabla, danioTabla, "Guerrero");
    }//GEN-LAST:event_botonGuerreroActionPerformed

    private void btnCombateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCombateActionPerformed
        Combate combate = new Combate();
        combate.iniciarCombate();
    }//GEN-LAST:event_btnCombateActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable ataqueTabla;
    private javax.swing.JButton botonGuerrero;
    private javax.swing.JButton botonMago;
    private javax.swing.JButton btnCombate;
    private javax.swing.JTable danioTabla;
    private javax.swing.JScrollPane jScrollPaneAtaque;
    private javax.swing.JScrollPane jScrollPaneDaño;
    // End of variables declaration//GEN-END:variables
}
