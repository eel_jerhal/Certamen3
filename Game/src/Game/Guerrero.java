package Game;

import java.util.Random;

public class Guerrero extends Personaje {

    private String arma;

    public Guerrero(String nombre, String arma) {
        super(nombre, 150);
        this.arma = arma;
    }

    public String getArma() {
        return arma;
    }

    public void setArma(String arma) {
        this.arma = arma;
    }

    public synchronized void combatir() {
        Random rand = new Random();
        Integer ataque = rand.nextInt(4);
        switch (ataque) {
            case (0): {
                this.setAtaque("Golpe directo");
                this.setDannio(20);
                break;
            }
            case (1): {
                this.setAtaque("Giro letal");
                this.setDannio(30);
                break;
            }
            case (2): {
                this.setAtaque("Super Arma");
                this.setDannio(70);
                consumirEnergia(this.getDannio() * (int) 0.3);
                break;
            }
            case (3): {
                this.setAtaque("Pierde Turno");
                this.setDannio(0);
                break;
            }
        }
    }

    @Override
    public String toStringAtaque() {
        return "Guerrero;" + this.getAtaque() + ";" + this.getDannio();
    }

    @Override
    public String toStringDannio(Integer dannioRecibido) {
        return "Guerrero;" + dannioRecibido;
    }
}
