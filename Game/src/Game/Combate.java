package Game;

import GUI.Interfaz;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Combate {

    public void iniciarCombate() {
        //Se instancian los personajes
        Guerrero guerrero = new Guerrero("Nacho", "Espada");
        Mago mago = new Mago("Cindy", "Baston");

        //Se instancian los hilos para cada personaje
        ThreadCombatir combatir = new ThreadCombatir(guerrero, mago);
        ThreadEncantar encantar = new ThreadEncantar(mago, guerrero);

        //Se inician los hilos
        encantar.start();
        combatir.start();

        try {
            encantar.join();
            combatir.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (mago.getEnergia() <= 0) {
            System.out.println("El mago ha caído\n");
        } else {
            System.out.println("El guerrero ha caído\n");
        }
    }
}
